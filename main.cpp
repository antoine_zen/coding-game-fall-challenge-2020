#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

 int possessedIngredients[4];


 class Delta{
     public:
     Delta(){}
     Delta(int id, int delta0, int delta1, int delta2, int delta3){
         this->id = id;
         this->delta[0] = delta0;
         this->delta[1] = delta1;
         this->delta[2] = delta2;
         this->delta[3] = delta3;
     }
     ~Delta(){}
     
     bool isDeltaPositive(int ingredient[4])const{
        bool positive = 1;
        for(int i=0; i<4; i++){
             if(this->delta[i] + ingredient[i] < 0){
                
                positive = 0;
            }
            //cerr<<"inv"<<this->delta[i] <<"  "<< ingredient[i]<<endl;
        }
        //cerr<<"positive "<<positive<<endl;
        return positive;
     }
     
     int getMaxDelta() const{
         int complexity = 0;

         for (int i=0; i<4; i++){
             if(this->delta[i] != 0)
                complexity = i;
         }
         return complexity;
     }
         
     int getId()const{return this->id;}
     int getDelta(int i)const{return this->delta[i];}
     private:
     int id;
     int delta[4];
 };

 class Spell:public Delta{
     public:
     Spell(){}
     Spell(int spellId, int delta0, int delta1, int delta2, int delta3, int castable):
        Delta(spellId,delta0,delta1,delta2,delta3){
         this->castable = castable;
     }
     ~Spell(){}

     bool isCastable()const{
         
         int totalIngredient = 0;

         for(int i=0;i<4;i++)
            totalIngredient += possessedIngredients[i] + this->getDelta(i);

         bool haveIngredients = this->isDeltaPositive(possessedIngredients);

         cerr<<"is Catable "<< this->castable << "  "<< (totalIngredient <= 10)<<"  "<<haveIngredients<<endl;
         
         return this->castable && totalIngredient <= 10 && haveIngredients;}
     private:
     bool castable;
 };

 class Potion:public Delta{
     public:
     Potion(){}
     Potion(int potionID, int delta0, int delta1, int delta2, int delta3, int price):
        Delta(potionID,delta0,delta1,delta2,delta3){
         this->price = price;
         this->complexity = this->getMaxDelta();

     }
     ~Potion(){}

     int isBrewable(int ingredients[4])const {    
         //returns if potion is brewable if not -1, if possible returns price
         int delta = this->isDeltaPositive(ingredients);
         //cerr<<"delta"<<delta<<endl;
         if(delta>0)
             return delta;
         else 
             return 0;
     }

    
  
     int getPrice() const{return this->price;}
     int getComplexity() const{return this->complexity;}
     private:
     int price;
     int complexity;
     
 };

vector<Potion> potionList;
vector<vector<Spell>> spellList(4);
vector<Spell> bookList;

void get_input_data(){
    int actionCount; // the number of spells and recipes in play
    cin >> actionCount; cin.ignore();

    
    potionList.resize(0);
    
    for(int i=0;i<4;i++)
        spellList[i].resize(0);

    
    
    for (int i = 0; i < actionCount; i++) {
        int actionId; // the unique ID of this spell or recipe
        string actionType; // in the first league: BREW; later: CAST, OPPONENT_CAST, LEARN, BREW
        int delta0; // tier-0 ingredient change
        int delta1; // tier-1 ingredient change
        int delta2; // tier-2 ingredient change
        int delta3; // tier-3 ingredient change
        int price; // the price in rupees if this is a potion
        int tomeIndex; // in the first two leagues: always 0; later: the index in the tome if this is a tome spell, equal to the read-ahead tax; For brews, this is the value of the current urgency bonus
        int taxCount; // in the first two leagues: always 0; later: the amount of taxed tier-0 ingredients you gain from learning this spell; For brews, this is how many times you can still gain an urgency bonus
        bool castable; // in the first league: always 0; later: 1 if this is a castable player spell
        bool repeatable; // for the first two leagues: always 0; later: 1 if this is a repeatable player spell
        cin >> actionId >> actionType >> delta0 >> delta1 >> delta2 >> delta3 >> price >> tomeIndex >> taxCount >> castable >> repeatable; cin.ignore();

        int delta[4];
        delta[0] = delta0;
        delta[1] = delta1;
        delta[2] = delta2;
        delta[3] = delta3;
        if(actionType == "BREW")
            potionList.push_back(Potion(actionId, delta0, delta1, delta2, delta3,price));
        else if(actionType == "CAST"){
            for(int i = 0; i<4; i++){
                if(delta[i]>0){
                    //cerr<<actionId<<endl;
                    spellList[i].push_back(Spell(actionId, delta0, delta1, delta2, delta3,castable));
                }
            }
            
        }
        else if(actionType == "LEARN")
            bookList.push_back(Spell(actionId, delta0, delta1, delta2, delta3,castable));
    }
    
    int inv0; // tier-0 ingredients in inventory
    int inv1;
    int inv2;
    int inv3;
    int score; // amount of rupees
    cin >> inv0 >> inv1 >> inv2 >> inv3 >> score; cin.ignore();
    possessedIngredients[0] = inv0;
    possessedIngredients[1] = inv1;
    possessedIngredients[2] = inv2;
    possessedIngredients[3] = inv3;
    cin >> inv0 >> inv1 >> inv2 >> inv3 >> score; cin.ignore();
}

int cast_ingredient(int spellPos){

    //cerr<<"Spellpos"<<spellPos<<endl;

    if(spellPos < 0){
        return -1;
    }
    
    else if (spellPos == 0) {
        if(possessedIngredients[0]<3 && spellList[spellPos][0].isCastable()){
            return spellList[spellPos][0].getId();
        }
        else return -1;
    }
    
    else{
        int i = spellList[spellPos].size() -1;
        //cerr<< "while"<<i<<"size "<<spellList[spellPos].size()<<endl;
        while(i>0 && !spellList[spellPos][i].isCastable()){          
            i--;
        }

        if(spellList[spellPos][i].isCastable()){
            return spellList[spellPos][i].getId();
        }
        else{
            return cast_ingredient(spellPos-1);
        }
    }

}

string brewPotion(Potion selectedPotion){
    int i = 3;

    cerr<<"potion "<<selectedPotion.getId()<<endl;

    while(possessedIngredients[i] + selectedPotion.getDelta(i)>= 0){
        i--;
    }
    if(selectedPotion.isBrewable(possessedIngredients)){
        return "BREW " + to_string(selectedPotion.getId());
    }
    else{

        //cerr<<"ingredient "<< i <<endl;

        int spellID = cast_ingredient(i);

        //cerr<<"spell id"<< spellID<<endl;

        if(spellID>0)
            return "CAST " + to_string(spellID);

        else{
            return "REST";
        }
                    
    }
}



int main()
{
    // game loop
    get_input_data();
    cout<<"LEARN "<<bookList[0].getId()<<endl;
    get_input_data();
    cout<<"LEARN "<<bookList[1].getId()<<endl;
    get_input_data();
    cout<<"LEARN "<<bookList[2].getId()<<endl;
    /*get_input_data();
    cout<<"LEARN "<<bookList[3].getId()<<endl;
    get_input_data();
    cout<<"LEARN "<<bookList[4].getId()<<endl;*/
        
    while (1) {
        get_input_data();

        int id = 0;
        float selectedscore = 0;
        int position = 0;

        for(int i=0;i<5;i++){
            float potionscore = potionList[i].getPrice()/23.f - 1.5 * (potionList[i].getComplexity()/4.f); 

            if( potionscore > selectedscore){
                position = i;
                id = potionList[i].getId();
                selectedscore = potionscore;
            }
        }
        cout<<brewPotion(potionList[position])<<endl;
    }
        
      
}
